describe('Los estudiantes under monkeys', function() {
    it('visits los estudiantes and survives monkeys', function() {
        cy.visit('https://losestudiantes.co');
        cy.contains('Cerrar').click();
        cy.wait(1000);
	randomClick(10);
        randomEvent(10);
    })
})
function randomClick(monkeysLeft) {

    function getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    };

    var monkeysLeft = monkeysLeft;
    if(monkeysLeft > 0) {
        cy.get('a').then($links => {
            var randomLink = $links.get(getRandomInt(0, $links.length));
            if(!Cypress.Dom.isHidden(randomLink)) {
                cy.wrap(randomLink).click({force: true});
                monkeysLeft = monkeysLeft - 1;
            }
            setTimeout(randomClick, 1000, monkeysLeft);
        });
    }   
}

function randomEvent(monkeysLeft) {

    function getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    };

    var monkeysLeft = monkeysLeft;
    if (monkeysLeft > 0) {

        var inputs = cy.get('input');

        if (inputs != null && inputs.length > 0) {
            inputs.then($inputs => {
                var input = $inputs.get(getRandomInt(0, $inputs.length));
                if (!Cypress.Dom.isHidden(input)) {
                    if (input != null && input.type == 'text') {
                        cy.wrap(input).click({ force: true }).type("test", { force: true });
                      
                    }
                }
                monkeysLeft = monkeysLeft - 1;
                setTimeout(randomEvent, 1000, monkeysLeft);
            });
        }

        cy.get('button').then($buttons => {
            var button = $buttons.get(getRandomInt(0, $buttons.length));
            if (!Cypress.Dom.isHidden(button)) {
                cy.wrap(button).click({ force: true });
                monkeysLeft = monkeysLeft - 1;
            }
            setTimeout(randomEvent, 1000, monkeysLeft);
        });

        var sels = cy.get('select');
        if (sels != null && sels.length > 0) {
            sels.then($selects => {
                var select = $selects.get(getRandomInt(0, $selects.length));
                if (!Cypress.Dom.isHidden(select)) {
                    cy.wrap(select).select(5, { force: true });
                    monkeysLeft = monkeysLeft - 1;
                }
                setTimeout(randomEvent, 1000, monkeysLeft);
            });
        }
    }
}