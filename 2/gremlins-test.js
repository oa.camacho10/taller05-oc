function loadScript(callback) {
	var s = document.createElement('script');
	s.src = 'https://rawgithub.com/marmelab/gremlins.js/master/gremlins.min.js';
	if(s.addEventListener) { 
		s.addEventListener('load',callback,false);
	} else if(s.readyState) {
		s.onreadystatechange = callback
	}
	document.body.appendChild(s);
}

function unleashGremlins(ttl, callback) {
    function stop() {
        horde.stop();
        callback();
    }

    var formFillerGremlin = gremlins.species.formFiller();    
    formFillerGremlin.canFillElement(function(element) { return true }); // to limit where the gremlin can fill

    var clickerGremlin = gremlins.species.clicker();
    clickerGremlin.clickTypes(['click']); // the mouse event types to trigger
    clickerGremlin.canClick(function(element) { return true }); // to limit where the gremlin can click

    var distributionStrategy = gremlins.strategies.distribution();
    distributionStrategy.delay(10); 
    distributionStrategy.distribution([0.6, 0.4]); 

    var horde = window.gremlins.createHorde();
    horde.gremlin(clickerGremlin);
    horde.gremlin(formFillerGremlin);
    horde.strategy(distributionStrategy);
    horde.seed(1234);

    horde.after(callback);
    window.onbeforeunload = stop;
    setTimeout(stop, ttl);
    horde.unleash();
}

describe('Monkey testing with gremlins ', function () {
  
  it('it should not raise any error', function () {
    browser.url('/');
    browser.click('button=Cerrar');
    
    browser.timeoutsAsyncScript(60000);
    browser.executeAsync(loadScript);
   
    browser.timeoutsAsyncScript(60000);
    browser.executeAsync(unleashGremlins, 50000);
  });

  afterAll(function() {
	 browser.log('browser').value.forEach(function(log) { 
		 browser.logger.info(log.message.split(' ')[2]);
	 ;});
  });

});
